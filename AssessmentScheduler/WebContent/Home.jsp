<%@page import="com.mvc.bean.SessionBean"%>
<%@page language="java" import="java.util.*"%>
<html>
   <head>
      <title>Home Page</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="assessment.css">
      <script src="MyJavaScript.js"></script>
      <script>
         history.forward();
      </script>
      <style>
         #dvLoading{
         background:url(images/spinner.gif) no-repeat center center;
         height: 200px;
         width: 200px;
         position: fixed;
         z-index: 1000;
         left: 40%;
         top: 20%;
         }
      </style>
      <script>
         function onClickOfRegisterButton(value){
          document.getElementById("fadeOutDiv").disabled=true;
          document.getElementById("dvLoading").style.display = "block";
          
          window.location="RegisterServlet?registerbutton=" + value;
         }
      </script>
      <%
         HttpSession roleSession = request.getSession();
         int role = (Integer) roleSession.getAttribute("rolesession");
         HttpSession namesession = request.getSession();
         String fullname = roleSession.getAttribute("namesession").toString();
         %>
   </head>
   <body id="fadeOutDiv">
      <div id="dvLoading" style="display:none;">
      </div>
      <%
         if (role == 3) {
         %>
      <%@include file="Header.html"%>
      <%
         } else {
         %>
      <%@include file="StudentHeader.html"%>
      <%
         }
         %>
      <br>
      <br>
      <img src="images/loading.gif" id="img" style="display:none" />
      <div class="col-md-8 col-md-offset-2">
         <div class="bg-main">
            <div class="page-header">
               <h2>
                  Welcome
                  <%=fullname%>
               </h2>
            </div>
            <%
               if(request.getAttribute("Incorrect")!=null) {
               %>
            <div class="alert alert-danger">
               <%=request.getAttribute("Incorrect")%>
            </div>
            <%
               }
               %>
            <div id="MainContent_registeredMeetingsContainer">
               <div class="panel panel-primary" id="registeredMeetingsPanel">
                  <div class="panel-heading">
                     <strong class="h3">Registered Assessments</strong>
                  </div>
                  <div>
                     <table class="table table-hover" cellspacing="0" style="border-collapse: collapse; table-layout: fixed; word-wrap: break-word;">
                        <%
                           List<SessionBean> reglist = (List<SessionBean>)request.getAttribute("registeredSessionsList");
                           									if(reglist.size()!=0)
                           									{
                           %>
                        <tr>
                           <th class="bg-primary" scope="col" style="border-width: 0px;">Assessment Name
                           </th>
                           <th class="bg-primary" scope="col" style="border-width: 0px;">Date</th>
                           <th class="bg-primary" scope="col" style="border-width: 0px;">Description</th>
                           <th class="bg-primary" scope="col" style="border-width: 0px;">&nbsp;</th>
                        </tr>
                        <%
                           for(SessionBean sb: reglist)
                           									{ 
                           									int sessionRegID=0,assessRegID=0;
                           									String sessionStart=sb.getSessionStart().toString().trim();
                           									sessionStart=sessionStart.substring(0, sessionStart.length() - 2);
                           %>
                        <tr>
                           <td>
                              <%=sb.getAssessmentName()%>
                           </td>
                           <td>
                              <%=sessionStart%>
                           </td>
                           <td>
                              <%=sb.getAssessmentDesc()%>
                           </td>
                           <td style="display: none;">
                              <%=sessionRegID=sb.getSessionID()%>
                           </td>
                           <td style="display: none;">
                              <%=assessRegID=sb.getAssessmentID()%>
                           </td>
                           <td>
                              <div>
                                 <form action="RegisterServlet" method="post">
                                    <button class="btn btn-danger pull-left" id="unregisterButton">UnRegister</button>
                                    <input type="hidden" name="values" value="<%=sessionRegID+"/"+assessRegID+"/"+"unregisterbutton"%>" />
                                 </form>
                                 <form action="DetailsServlet" method="post" id="details">
                                    <button type="submit" id="details" class="btn btn-success">Details</button>
                                    <input type="hidden" value="<%=sessionRegID %>" name="sessionid">
                                    <input type="hidden" value="<%=assessRegID %>" name="assessmentid">
                                    <input type="hidden" id="false" value="false" name="false">
                                 </form>
                              </div>
                           </td>
                        </tr>
                        <%
                           }
                           }
                           else{
                           %>
                        <tr>
                           <td class="alert alert-info" role="alert">Nothing seems to be here. Register for a session and it will show up here.</td>
                        </tr>
                        <%
                           }
                           %>
                     </table>
                  </div>
               </div>
            </div>
            <div id="UpcominMeetings">
               <div class="panel panel-primary">
                  <div class="panel-heading">
                     <strong class="h3">Upcoming Assessments</strong>
                  </div>
                  <div>
                     <table class="table table-hover" cellspacing="0" style="border-collapse: collapse; table-layout: fixed; word-wrap: break-word;">
                        <tr>
                           <th class="bg-primary" scope="col" style="border-width: 0px;">Assessment Name
                           </th>
                           <th class="bg-primary" scope="col" style="border-width: 0px;">Date</th>
                           <th class="bg-primary" scope="col" style="border-width: 0px;">Description</th>
                           <th class="bg-primary" scope="col" style="border-width: 0px;">&nbsp;</th>
                        </tr>
                        <%
                           List<SessionBean> dataassess = (List<SessionBean>) request.getAttribute("upcomingSessionsList");
                           										for(SessionBean sb: dataassess){
                           											int sessionID=0,assessID=0,noofparticipantsallowed=0,noofparticipantsregistered=0;
                           										String sessionStart=sb.getSessionStart().toString().trim();
                           										sessionStart=sessionStart.substring(0, sessionStart.length() - 2);
                           %>
                        <tr>
                           <td>
                              <%=sb.getAssessmentName()%>
                           </td>
                           <td>
                              <%=sessionStart%>
                           </td>
                           <td>
                              <%=sb.getAssessmentDesc()%>
                           </td>
                           <td style="display: none;">
                              <%=sessionID=sb.getSessionID()%>
                           </td>
                           <td style="display: none;">
                              <%=noofparticipantsregistered=sb.getNumOfParticipantsEnrolled()%>
                           </td>
                           <td style="display: none;">
                              <%=assessID=sb.getAssessmentID()%>
                           </td>
                           <td style="display: none;">
                              <%=noofparticipantsallowed=sb.getNumOfParticipants()%>
                           </td>
                           <td>
                              <%
                                 if(noofparticipantsregistered==noofparticipantsallowed)
                                 												{
                                 %>
                              <form action="RegisterServlet" method="post" name="registerForm">
                                 <button value="<%=sessionID+"/"+assessID+"/"+"registerbutton"%>" class="btn btn-danger pull-left"
                                    id="registerButton" onclick="onClickOfRegisterButton(this.value)" disabled="true">Register</button>
                                 <input type="hidden" name="values" value="<%=sessionID+"/"+assessID+"/"+"registerbutton"%>" />
                              </form>
                              <%
                                 } else{
                                 %>
                              <div>
                                 <form action="RegisterServlet" method="post" name="registerForm">
                                    <button value="<%=sessionID+"/"+assessID+"/"+"registerbutton"%>" class="btn btn-primary pull-left"
                                       id="registerButton" onclick="onClickOfRegisterButton(this.value)">Register</button>
                                    <input type="hidden" name="values" value="<%=sessionID+"/"+assessID+"/"+"registerbutton"%>" />
                                 </form>
                                 <%
                                    }
                                    %>
                                 <form action="DetailsServlet" method="post" id="details">
                                    <button type="submit" id="details" class="btn btn-success">Details</button>
                                    <input type="hidden" value="<%=sessionID %>" name="sessionid">
                                    <input type="hidden" value="<%=assessID %>" name="assessmentid">
                                    <input type="hidden" id="true" value="true" name="true">
                                 </form>
                              </div>
                           </td>
                        </tr>
                        <%
                           }
                           %>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <br>
      <br>
      
   </body>
</html>