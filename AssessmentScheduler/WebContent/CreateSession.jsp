<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Create session</title>
      <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <script
         src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script
         src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="assessment.css">
      <link rel="stylesheet"
         href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <link rel="stylesheet" href="/resources/demos/style.css">
      <link rel="stylesheet"
         href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <script
         src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
      <script src="MyJavaScript.js"></script>
      <script>
         history.forward();
      </script>
      <script>
         $(function() {
         	var dateToday = new Date();
         	$("#startdatepicker").datepicker({
         		numberOfMonths : 1,
         		showButtonPanel : true,
         		minDate : dateToday
         	});
         	$("#enddatepicker").datepicker({
         		numberOfMonths : 1,
         		showButtonPanel : true,
         		minDate : dateToday
         	});
         	$("#starttimepicker").timepicker();
         	$("#endtimepicker").timepicker();
         });
         
         function validate() {
         
         	event.preventDefault();
         
         	if (document.createAForm.startDate.value == "") {
         
         		alert("Please enter the startDate ");
         		document.createAForm.startDate.focus();
         		return false;
         	} else if (document.createAForm.startTime.value == "") {
         		alert("Please enter the startTime");
         		document.createAForm.startTime.focus();
         		return false;
         	}
         
         	else if (document.createAForm.endDate.value == "") {
         		alert("Please enter the endDate");
         		document.createAForm.endDate.focus();
         		return false;
         	} else if (document.createAForm.endTime.value == "") {
         		alert("Please enter the endTime");
         		document.createAForm.endTime.focus();
         		return false;
         	}
         
         	else if (document.createAForm.numOfParticipants.value == "") {
         		alert("Please enter the numOfParticipants");
         		document.createAForm.numOfParticipants.focus();
         		return false;
         	} else {
         		document.createAForm.submit();
         	}
         
         }
      </script>
   </head>
   <body>
      <div class="container">
      <div class="row">
         <div id="MainContentContainer" class="col-md-12">
            <div class="row">
               <div class="col-xs-12">
                  <h2 class="panel-heading"></h2>
               </div>
            </div>
            <div class="bg-main">
               <div class="col-md-8 col-md-offset-2">
                  <div class="panel panel-primary" id="createMeetingForm">
                     <div class="panel-heading">
                        <strong id="MainContent_panelHeader" class="h1">Create
                        Session</strong> <strong class="h3"> <span
                           id="MainContent_WSNameLBL"></span></strong>
                     </div>
                     <div class="panel-body grid">
                        <form name="createAForm" action="SessionServlet">
                           <div class="form-group col-md-12">
                              <div id="nameFormGroup" class="form-group">
                                 <br> <br> <label for="AsName">You are
                                 creating session for below assessment:</label> 
                                 <%HttpSession sess = request.getSession(); 
                                    %>
                                 <%-- <label for="WSName"><%=sess.getAttribute("assessmentName")%></label> --%>
                                 <input type="text"
                                    value="<%=sess.getAttribute("assessmentName")%>"
                                    class="form-control" readonly>
                                 <input type="hidden" name="assessmentID" value="<%=sess.getAttribute("assessmentId")%>">
                              </div>
                           </div>
                           <div class="row-fluid form-group">
                              <div class="form-group col-md-3">
                                 <label for="startdatepicker">Start Date:</label>
                                 <div id="startdatepickerr" class="input-group">
                                    <input name="startDate" type="text" id="startdatepicker"
                                       class="form-control">
                                 </div>
                              </div>
                              <div class="form-group col-md-3">
                                 <label for="starttimepicker">Start Time:</label>
                                 <div id="starttimepickerr" class="input-group">
                                    <input name="startTime" type="text" id="starttimepicker"
                                       class="form-control">
                                 </div>
                              </div>
                              <div class="form-group col-md-3">
                                 <label for="enddatepicker">End Date:</label>
                                 <div id="enddatepickerr" class="input-group date">
                                    <input name="endDate" type="text" id="enddatepicker"
                                       class="form-control">
                                 </div>
                              </div>
                              <div class="form-group col-md-3">
                                 <label for="endtimepicker">End Time:</label>
                                 <div id="endtimepickerr" class="input-group">
                                    <input name="endTime" type="text" id="endtimepicker"
                                       class="form-control">
                                 </div>
                              </div>
                              <div class="form-group col-md-12">
                                 <span id="dateTimeValidation"
                                    style="color: red; display: none;">Please select a
                                 valid start time and End time.</span>
                              </div>
                           </div>
                           <div class="row-fluid form-group">
                              <div class="form-group col-md-6">
                                 <div class="form-group">
                                    <label for="buildingsDropdown">Building:</label> 
                                    <select
                                       id="buildingsDropdown" name="building"
                                       class="form-control c-select">
                                       <option value="0">No Preference</option>
                                       <option value="GS">Garrett-Strong</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-6 form-group">
                                 <label for="participantTextBox">Number of
                                 participants allowed:</label> <input type="text"
                                    id="participantTextBox" name="numOfParticipants"
                                    class="form-control">
                              </div>
                              <div class="col-md-6 form-group">
                                 <input name="assessid" type="hidden" id="assessid"
                                    class="form-control">
                              </div>
                        </form>
                        <div class="row-fluid">
                        <div class="col-md-12 btn-toolbar">
                        <button class="btn btn-success pull-right" id="createButton"
                           onClick="return validate(event)">Create</button>
                        <form action="SessionServlet" method="post">
                        <button class="btn btn-defualt pull-left"
                           value="<%=request.getParameter("assessmentid") %>"
                           name="sessionButton">Cancel</button>
                        </form>
                        </div>
                        </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>