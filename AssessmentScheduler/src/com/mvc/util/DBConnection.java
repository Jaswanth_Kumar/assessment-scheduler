package com.mvc.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

	public static Connection createConnection() throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
        //cite7 credentials
		/*String url = "jdbc:mysql://127.0.0.1:3306/assessments";
        String user = "citedev";
        String password = "h0nMY421K6Z$bT";*/
        
        //cite 1 credentials
        String url = "jdbc:mysql://cite1.nwmissouri.edu:3306/assessments";
        String user = "citeuser";
        String password = "citeuser";
        
        
        Connection con = DriverManager.getConnection(url, user, password);
       

		return con;
	}
}