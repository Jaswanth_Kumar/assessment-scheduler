package com.mvc.dao;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mvc.bean.SessionBean;
import com.mvc.util.DBConnection;

public class DetailsDao {
	
	public static List<SessionBean> getsession(String sessionid) throws ClassNotFoundException
	{
		Connection con = null;
		Statement statement = null;
		ResultSet resultSet = null;
		List<SessionBean> List = new ArrayList<>();
		try {
			con = DBConnection.createConnection();
			statement = con.createStatement();			
			resultSet = statement.executeQuery("select assessments.assessmentName, assessments.assessmentDesc, sessions.sessionStart, sessions.sessionEnd, sessions.sessionID, sessions.assessmentID, "
					+ " building.buildingName, sessions.roomNumber "
					+ "from assessments, sessions, building  where assessments.assessmentID =sessions.assessmentID  and sessions.sessionID="+sessionid);	
			while(resultSet.next())
			{
				SessionBean sb = new SessionBean();
				sb.setAssessmentName(resultSet.getString("assessments.assessmentName"));
				sb.setAssessmentDesc(resultSet.getString("assessments.assessmentDesc"));
				sb.setSessionStart(resultSet.getTimestamp("sessions.sessionStart"));
				sb.setSessionEnd(resultSet.getTimestamp("sessions.sessionEnd"));
				sb.setBuildingCode(resultSet.getString("building.buildingName"));
				sb.setRoomNumber(String.valueOf(resultSet.getInt("sessions.roomNumber")));
				sb.setSessionID(resultSet.getInt("sessions.sessionID"));
				sb.setAssessmentID(resultSet.getInt("sessions.assessmentID"));
				List.add(sb);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return List;
	}
	

	public static void register(String sessionid, String assessmentID, Date dateRegistered, String userID) throws ClassNotFoundException
	{
		Connection conn = null;
		PreparedStatement ps = null;
		String register ="insert into assessments.enrollments (sessionID,assessmentID,dateRegistered,userID) values(?,?,?,?);";
		int updateQuery = 0;
		try
		{
			conn = DBConnection.createConnection();
			ps = conn.prepareStatement(register);
			ps.setString(1,sessionid);
			ps.setString(2,assessmentID);
			ps.setDate(3, (Date) dateRegistered);
			ps.setString(4, userID);
			updateQuery = ps.executeUpdate();
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String compare(int assessmnetID) throws ClassNotFoundException
	{
		Connection con = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String sessionid = "";
		List<Object> List = new ArrayList<>();
		try {
			con = DBConnection.createConnection();
			statement = con.createStatement();
			//resultSet = statement.executeQuery("select sessionid from assessments.enrollments where assessmentID ="+assessmnetID+";");
			sessionid = "select sessionid from assessments.enrollments where assessmentID ="+assessmnetID+";";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sessionid;
	}
}
