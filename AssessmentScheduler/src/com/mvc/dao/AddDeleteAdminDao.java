package com.mvc.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mvc.util.DBConnection;
public class AddDeleteAdminDao {
	
	public static String insertAdmin(String adminId)
			throws ClassNotFoundException {
		Connection con = null;
		PreparedStatement statement = null;
		String text="";
		try {

			con = DBConnection.createConnection();
			int count=verifyAdmin(adminId);
			if(count==0)
			{
			String query = "insert into admin(adminUserID,role) values(?,?)";
			statement = con.prepareStatement(query);
			statement.setString(1, adminId);
			statement.setInt(2, 3);
			statement.executeUpdate();
			text=adminId+" is added as admin successfully";
			}
			else
			{
				text=adminId+" is already an admin ";
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return text;
	}
	
	
	public static String deleteAdmin(String adminId)
			throws ClassNotFoundException {

		Connection con = null;
		Statement statement = null;
		String text="";
		try {

			con = DBConnection.createConnection();
			statement = con.createStatement();
			int count=verifyAdmin(adminId);
			if(count==1)
			{
			String query = "delete from admin where adminUserID='"+ adminId+ "'";
			 statement.executeUpdate(query);
			 text=adminId+" is deleted as admin successfully";
			}
			else
			{
				text=adminId+" is not an admin ";
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return text;
	}
	
	
	public static int verifyAdmin(String adminId) throws ClassNotFoundException {
		Connection con = null;
		Statement statement = null;
		ResultSet resultSet = null;
		int count = 0;
		List<Object> list = new ArrayList<>();
		try {

			con = DBConnection.createConnection();
			statement = con.createStatement();
			String query = "select count(*) from admin where adminUserID= '"+ adminId+ "'";

			resultSet = statement.executeQuery(query);

			while (resultSet.next()) {

				count = resultSet.getInt(1);

			}
			con.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}
	
	

}
