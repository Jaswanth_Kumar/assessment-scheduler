package com.mvc.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mvc.bean.SessionBean;
import com.mvc.dao.DetailsDao;

public class DetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 9112360638408569328L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException,
			ClassNotFoundException, SQLException {

		HttpSession session = request.getSession();
		if (session != null && LoginServlet.isLogin) {
			List<SessionBean> list = new ArrayList<>();
			List<Object> clist = new ArrayList<Object>();
			String assesIDFromJSp = request.getParameter("sessionid");
			String tr = request.getParameter("true");
			String fa = request.getParameter("false");
			try {
				list = DetailsDao.getsession(assesIDFromJSp);
				clist.add(tr);
				clist.add(fa);
				request.setAttribute("sessionList", list);
				request.setAttribute("checklist", clist);
				request.getRequestDispatcher("/Details.jsp").forward(request,
						response);
			} catch (Exception e) {
			}
		} else {
			request.getRequestDispatcher("/Login.jsp").forward(request,
					response);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (ClassNotFoundException | SQLException ex) {
			ex.printStackTrace();
		}
	}
}
