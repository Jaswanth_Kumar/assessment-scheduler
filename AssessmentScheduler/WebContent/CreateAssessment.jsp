<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <script
         src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script
         src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="assessment.css">
      <script src="MyJavaScript.js"></script>
      <script>
         history.forward();
      </script>
      <script>
         function validate() {
         	event.preventDefault();
         	if (document.createAForm.assessmentName.value == "") {
         		alert("Please enter the assessment name");
         		document.createAForm.assessmentName.focus();
         		return false;
         	} else if (document.createAForm.assessmentDescription.value == "") {
         		alert("Please enter the description");
         		document.createAForm.assessmentDescription.focus();
         		return false;
         	} else {
         		document.createAForm.submit();
         	}
         
         }
      </script>
   </head>
   <body>
      <%@include file="Header.html"%>
      <br>
      <br>
      <br>
      <br>
      <div class="container">
         <div class="row">
            <div id="MainContentContainer" class="col-md-12">
               <div class="row">
                  <div class="col-xs-12">
                     <h2 class="panel-heading"></h2>
                  </div>
               </div>
               <div class="bg-main">
                  <div class="col-md-8 col-md-offset-2">
                     <div class="panel panel-primary" id="createMeetingForm">
                        <div class="panel-heading">
                           <strong id="MainContent_panelHeader" class="h1">Create
                           Assessment</strong>
                        </div>
                        <div class="panel-body grid">
                           <form name="createAForm" action="AssessmentServlet">
                              <div class="form-group col-md-12">
                                 <div id="nameFormGroup" class="form-group">
                                    <br> <br> <label for="WSName">Name:</label> <input
                                       type="text" name="assessmentName" class="form-control">
                                 </div>
                              </div>
                              <div class="form-group col-md-12">
                                 <div id="descriptionFormGroup" class="form-group">
                                    <label for="WSDescription">Description:</label>
                                    <textarea rows="5" cols="20" name="assessmentDescription"
                                       class="form-control"></textarea>
                                 </div>
                              </div>
                           </form>
                           <div class="row-fluid">
                              <div class="col-md-12 btn-toolbar">
                                 <button class="btn btn-success pull-right" id="createButton" onClick="return validate(event)">Create</button>
                                 <form action="AssessmentServlet" method="post">
                                    <button class="btn btn-defualt pull-left">Cancel</button>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>