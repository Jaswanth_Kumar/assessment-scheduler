package com.mvc.bean;

import java.sql.Date;
import java.sql.Timestamp;

public class AssessmentBean {

	private int assessmentID;
	private String assessmentName;
	private String assessmentDesc;
	private Timestamp dateCreated;
	private Timestamp dateModified;

	public AssessmentBean() {

	}

	public AssessmentBean(int assessmentID, String assessmentName,
			String assessmentDesc, Timestamp dateCreated, Timestamp dateModified) {
		this.assessmentID=assessmentID;
		this.assessmentName=assessmentName;
		this.assessmentDesc=assessmentDesc;
		this.dateCreated=dateCreated;
		this.dateModified=dateModified;

	}

	public int getAssessmentID() {
		return assessmentID;
	}

	public void setAssessmentID(int assessmentID) {
		this.assessmentID = assessmentID;
	}

	public String getAssessmentName() {
		return assessmentName;
	}

	public void setAssessmentName(String assessmentName) {
		this.assessmentName = assessmentName;
	}

	public String getAssessmentDesc() {
		return assessmentDesc;
	}

	public void setAssessmentDesc(String assessmentDesc) {
		this.assessmentDesc = assessmentDesc;
	}

	public Timestamp getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Timestamp getDateModified() {
		return dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	@Override
	public String toString() {
		return "assessmentID=" + assessmentID
				+ ", assessmentName=" + assessmentName + ", assessmentDesc="
				+ assessmentDesc + ", dateCreated=" + dateCreated
				+ ", dateModified=" + dateModified;
	}

}
