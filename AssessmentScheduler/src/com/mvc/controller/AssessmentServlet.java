package com.mvc.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mvc.bean.AssessmentBean;
import com.mvc.dao.AssessmentDao;

public class AssessmentServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2188782785368108825L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException,
			ClassNotFoundException {
		HttpSession session = request.getSession();
		if(session !=null && LoginServlet.isLogin){
		String SearchValue=null;
		//getting the value entered in the search bar in admin page
				String assessmentSearchText = request.getParameter("searchTextForAssessment");
				
		// code for creating an assessment
				
				//Fetching the entered assessment name and description from createAssessment.jsp page
		String assessmentNameFromJsp = request
				.getParameter("assessmentName");
		String assessmentDescFromJsp = request
				.getParameter("assessmentDescription");	
		List<AssessmentBean> assessmentsList = new ArrayList<>();
		
		if (assessmentNameFromJsp !=null && assessmentDescFromJsp !=null ) {
			AssessmentBean assessmentbean = new AssessmentBean();
			assessmentbean.setAssessmentName(assessmentNameFromJsp);
			assessmentbean.setAssessmentDesc(assessmentDescFromJsp);
			
			//Passing assessmentbean as parameter to createassessment method
			AssessmentDao.createAssessment(assessmentbean);
			//Fetching the list of assessments
			assessmentsList = AssessmentDao.getAllAssessments();		
			SearchValue="";
		}
		
		// getting all the assessments
else {
			
			// for getting the list of assessments in the admin page load
			if(assessmentSearchText==null||assessmentSearchText.isEmpty()){
	
				assessmentsList = AssessmentDao.getAllAssessments();
				SearchValue="";
			}
			// getting the list of assessments with the search text
			else{
				assessmentsList=AssessmentDao.getSearchAssessments(assessmentSearchText);
				SearchValue=assessmentSearchText;
				
			}
			
		}
		request.setAttribute("SearchValue",SearchValue);
		request.setAttribute("allAssessmentsList", assessmentsList);
		//Passing the list of assessments and search value to admin.jsp page
		request.getRequestDispatcher("/Admin.jsp").forward(request,
				response);
		}else {
			request.getRequestDispatcher("/Login.jsp").forward(request,
					response);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}
