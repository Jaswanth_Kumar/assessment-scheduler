package com.mvc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mvc.bean.AssessmentBean;
import com.mvc.util.DBConnection;

public class AssessmentDao {

	public static void createAssessment(AssessmentBean assessmentbean)
			throws ClassNotFoundException {
		Connection con = null;
		PreparedStatement statement = null;
		try {

			con = DBConnection.createConnection();
			String query = "insert into assessments(assessmentName,assessmentDesc,dateCreated,dateModified) values(?,?,?,?)";
			statement = con.prepareStatement(query);
			statement.setString(1, assessmentbean.getAssessmentName());
			statement.setString(2, assessmentbean.getAssessmentDesc());
			java.sql.Timestamp dateString = new java.sql.Timestamp(
					new java.util.Date().getTime());
			statement.setTimestamp(3, dateString);
			statement.setTimestamp(4, dateString);
			statement.executeUpdate();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static int getEnrollList(int sid) throws ClassNotFoundException {
		Connection con = null;
		Statement statement = null;
		ResultSet resultSet = null;
		int count = 0;
		List<Object> list = new ArrayList<>();
		try {

			con = DBConnection.createConnection();
			statement = con.createStatement();
			String query = "select count(*) from enrollments where sessionId= "
					+ sid;

			resultSet = statement.executeQuery(query);

			while (resultSet.next()) {

				count = resultSet.getInt(1);

			}
			con.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

	// for admin paage, list of assessments
	public static List<AssessmentBean> getAllAssessments()
			throws ClassNotFoundException {
		Connection con = null;
		Statement statement = null;
		ResultSet resultSet = null;
		List<AssessmentBean> assessmentsList = new ArrayList<>();
		try {

			con = DBConnection.createConnection();
			statement = con.createStatement();
			String query = "select assessmentID,assessmentName,assessmentDesc from assessments";
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				AssessmentBean ABean = new AssessmentBean();
				ABean.setAssessmentID(resultSet.getInt("assessmentID"));
				ABean.setAssessmentName(resultSet.getString("assessmentName"));
				ABean.setAssessmentDesc(resultSet.getString("assessmentDesc"));
				assessmentsList.add(ABean);
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return assessmentsList;
	}

	public static List<AssessmentBean> getAssessmentDetails(String assessmentId) throws ClassNotFoundException {
		Connection con = null;
		Statement statement = null;
		ResultSet resultSet = null;
		List<AssessmentBean> list = new ArrayList<>();
		try {

			con = DBConnection.createConnection();
			statement = con.createStatement();
			String query = "select assessments.assessmentID,assessments.assessmentName,assessments.assessmentDesc from assessments where assessments.assessmentID="+assessmentId;
			
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				AssessmentBean ABean = new AssessmentBean();
				ABean.setAssessmentID(resultSet.getInt("assessmentID"));
				ABean.setAssessmentName(resultSet.getString("assessmentName"));
				ABean.setAssessmentDesc(resultSet.getString("assessmentDesc"));
				list.add(ABean);
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public static List<AssessmentBean> getSearchAssessments(
			String assessmentSearchText) throws ClassNotFoundException {
		Connection con = null;
		Statement statement = null;
		ResultSet resultset = null;
		List<AssessmentBean> AList = new ArrayList<>();
		try {
			con = DBConnection.createConnection();
			statement = con.createStatement();
			String query = "select assessmentName,assessmentDesc,assessmentID from assessments where assessmentName like '%"
					+ assessmentSearchText + "%'";
			resultset = statement.executeQuery(query);
			while (resultset.next()) {
				AssessmentBean ABean = new AssessmentBean();
				ABean.setAssessmentName(resultset.getString("assessmentName"));
				ABean.setAssessmentDesc(resultset.getString("assessmentDesc"));
				ABean.setAssessmentID(resultset.getInt("assessmentID"));
				AList.add(ABean);
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return AList;
	}
}
