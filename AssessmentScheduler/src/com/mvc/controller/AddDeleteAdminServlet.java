package com.mvc.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mvc.dao.AddDeleteAdminDao;

public class AddDeleteAdminServlet extends HttpServlet {
	private static final long serialVersionUID = -8695390562771634939L;

	public AddDeleteAdminServlet() {
		super();
	}

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException,
			ClassNotFoundException {
		String lblText="";
		String addAdminText = request.getParameter("AddAdminText");
		String deleteAdminText = request.getParameter("DeleteAdminText");
		if(addAdminText!="")
		{
			 lblText=AddDeleteAdminDao.insertAdmin(addAdminText);
		}
		
		else
		{
			 lblText=AddDeleteAdminDao.deleteAdmin(deleteAdminText);
		}
		
		request.setAttribute("Message",lblText);
		//Passing the list of assessments and search value to admin.jsp page
		request.getRequestDispatcher("/AddAdmin.jsp").forward(request,
				response);

	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}
	}

}
