<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <script
         src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script
         src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="assessment.css">
      <script src="MyJavaScript.js"></script>
      <script>
         history.forward();
      </script>
   </head>
   <body>
      <%@include file="Header.html"%>
      <br>
      <br>
      <br>
      <br>
      <div class="container">
         <div class="row">
            <div id="MainContentContainer" class="col-md-12">
               <div class="row">
                  <div class="col-xs-12">
                     <h2 class="panel-heading"></h2>
                  </div>
               </div>
               
               
               <div class="bg-main">
                  <div class="col-md-8 col-md-offset-2">
                   <%
				if(request.getAttribute("Message")!=null) {
			%>
                                            <div class="alert alert-info">
                                                <%=request.getAttribute("Message")%>
                                            </div>
                                            <%
				}
			%>
                     <div class="panel panel-primary" id="createMeetingForm">
                        <div class="panel-heading">
                           <strong id="MainContent_panelHeader" class="h1">Add/Delete admin</strong>
                        </div>
                        <div class="panel-body grid">
                           <form method="post" action="AddDeleteAdminServlet">
                              <div class="form-group col-md-12">
                                 <div id="nameFormGroup" class="form-group">
                                    <br> <br> <label for="WSName">Add Admin:</label> 
                                    <input type="text" name="AddAdminText">
                                    <button class="btn btn-success pull-right">Add</button>
                                 </div>
                              </div>
                              <div class="form-group col-md-12">
                                 <div id="nameFormGroup" class="form-group">
                                    <br> <br> <label for="WSName">Delete Admin:</label> 
                                    <input type="text" name="DeleteAdminText">
                                    <button class="btn btn-danger pull-right" onClick="">Delete</button>
                                 </div>
                              </div>
                           </form>
                           <div class="row-fluid">
                              <div class="col-md-12 btn-toolbar">
                                 <form action="AssessmentServlet" method="post">
                                    <button class="btn btn-defualt pull-left">Back</button>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>