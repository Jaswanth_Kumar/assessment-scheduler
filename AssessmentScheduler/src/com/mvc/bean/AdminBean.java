package com.mvc.bean;

public class AdminBean {
	private String adminUserID;
	private String adminFirstName;
	private String adminLastName;

	public String getAdminUserID() {
		return adminUserID;
	}

	public void setAdminUserID(String adminUserID) {
		this.adminUserID = adminUserID;
	}

	public String getAdminFirstName() {
		return adminFirstName;
	}

	public void setAdminFirstName(String adminFirstName) {
		this.adminFirstName = adminFirstName;
	}

	public String getAdminLastName() {
		return adminLastName;
	}

	public void setAdminLastName(String adminLastName) {
		this.adminLastName = adminLastName;
	}

	@Override
	public String toString() {
		return "AdminBean [adminUserID=" + adminUserID + ", adminFirstName="
				+ adminFirstName + ", adminLastName=" + adminLastName + "]";
	}

}
