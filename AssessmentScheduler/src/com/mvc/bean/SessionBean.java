package com.mvc.bean;

import java.sql.Date;
import java.sql.Timestamp;

import com.mvc.bean.*;

public class SessionBean extends AssessmentBean {
	private int sessionID;
	private Timestamp sessionStart;
	private Timestamp sessionEnd;
	private Timestamp dateCreated;
	private Timestamp dateModified;
	private int numOfParticipants;
	private int numOfParticipantsEnrolled;
	private String buildingCode;
	private String roomNumber;

	private int assessmentID;
	private String assessmentName;
	private String assessmentDesc;
	private Timestamp assessDateCreated;
	private Timestamp assessDateModified;

	public SessionBean() {
	}

	public SessionBean(int sessionID, Timestamp sessionStart,
			Timestamp sessionEnd, Timestamp dateCreated,
			Timestamp dateModified, int numOfParticipants,int numOfParticipantsEnrolled,String buildingCode,String roomNumber,
			int assessmentID,String assessmentName, String assessmentDesc,
			Timestamp assessDateCreated, Timestamp assessDateModified) {
		super(assessmentID, assessmentName, assessmentDesc, assessDateCreated,
				assessDateModified);
		this.sessionID = sessionID;
		this.sessionStart = sessionStart;
		this.sessionEnd = sessionEnd;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.numOfParticipants = numOfParticipants;
		this.numOfParticipantsEnrolled=numOfParticipantsEnrolled;
		this.buildingCode=buildingCode;
		this.roomNumber=roomNumber;

	}

	public int getSessionID() {
		return sessionID;
	}

	public void setSessionID(int sessionID) {
		this.sessionID = sessionID;
	}

	public Timestamp getSessionStart() {
		return sessionStart;
	}

	public void setSessionStart(Timestamp sessionStart) {
		this.sessionStart = sessionStart;
	}

	public Timestamp getSessionEnd() {
		return sessionEnd;
	}

	public void setSessionEnd(Timestamp sessionEnd) {
		this.sessionEnd = sessionEnd;
	}

	public Timestamp getDateModified() {
		return dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	public Timestamp getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public int getNumOfParticipants() {
		return numOfParticipants;
	}

	public void setNumOfParticipants(int numOfParticipants) {
		this.numOfParticipants = numOfParticipants;
	}
	
	public int getNumOfParticipantsEnrolled() {
		return numOfParticipantsEnrolled;
	}

	public void setNumOfParticipantsEnrolled(int numOfParticipantsEnrolled) {
		this.numOfParticipantsEnrolled = numOfParticipantsEnrolled;
	}
	public String getBuildingCode() {
		return buildingCode;
	}

	public void setBuildingCode(String buildingCode) {
		this.buildingCode = buildingCode;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
	@Override
	public String toString() {
		return "SessionBean [sessionID=" + sessionID + ", sessionStart="
				+ sessionStart + ", sessionEnd=" + sessionEnd
				+ ", dateModified=" + dateModified + ", dateCreated="
				+ dateCreated + ", numOfParticipants=" + numOfParticipants
				+  super.toString()+"]";
	}

}
