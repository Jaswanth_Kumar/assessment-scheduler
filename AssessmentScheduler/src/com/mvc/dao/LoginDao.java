package com.mvc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mvc.bean.LoginBean;
import com.mvc.util.DBConnection;

public class LoginDao {

	public static int authenticateUser(LoginBean loginBean)
			throws ClassNotFoundException {

		String query = "SELECT role FROM admin WHERE adminUserID = '"
				+ loginBean.getUserName() + "';";
		ResultSet isAdminRS = null;
		Connection con = null;
		PreparedStatement ps = null;
		int role = 0;
		try {
			con = DBConnection.createConnection();
			ps = con.prepareStatement(query);
			isAdminRS = ps.executeQuery();

			while (isAdminRS.next()) {
				role = isAdminRS.getInt(1);

			}
			
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (con != null) {
					con.close();
				}
				if (ps != null) {
					ps.close();
				}
				if (isAdminRS != null) {
					isAdminRS.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return role;
	}
	
	public static int userexists(String username)
			throws ClassNotFoundException {

		String query = "SELECT count(*) FROM users WHERE userID = '"
				+ username + "';";
		ResultSet isAdminRS = null;
		Connection con = null;
		PreparedStatement ps = null;
		int role = 0;
		try {
			con = DBConnection.createConnection();
			ps = con.prepareStatement(query);
			isAdminRS = ps.executeQuery();

			while (isAdminRS.next()) {
				role = isAdminRS.getInt(1);

			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (con != null) {
					con.close();
				}
				if (ps != null) {
					ps.close();
				}
				if (isAdminRS != null) {
					isAdminRS.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return role;
	}
	
public static void insertnewUser(String username,String fname,String lname) throws ClassNotFoundException {
		
	Connection con = null;
	PreparedStatement statement = null;
	try {

		con = DBConnection.createConnection();
		String query = "insert into users(userID,firstName,lastName,registrationDate) values(?,?,?,?)";
		statement = con.prepareStatement(query);

		statement.setString(1, username);
		statement.setString(2, fname);
		statement.setString(3, lname);
		java.sql.Timestamp dateString = new java.sql.Timestamp(
				new java.util.Date().getTime());
		
		statement.setTimestamp(4, dateString);
		
		statement.executeUpdate();
		con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	
	}

}