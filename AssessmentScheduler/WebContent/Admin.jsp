<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<%@page language="java" import="java.util.*"%>
<%@page language="java" import="com.mvc.bean.AssessmentBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="assessment.css">
      <script src="MyJavaScript.js"></script>
      <script>
         history.forward();
      </script>
      <script>
         function callConfirmDelete(value){
         	
         	$('#myModal').modal('show');
         	$('#btnYes').click(function(){
         		$('#myModal').modal('hide');
         		window.location="SessionServlet?values=" + value;
         	});
         }
         
      </script>
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   </head>
   <body>
      <%@include file="Header.html"%>
      <br>
      <br>
      <br>
      <br>
      <div style="margin-right: 2em">
                                <button type="button" class="btn btn-success pull-right" onclick="openPage('AddAdmin.jsp')">
                                                    <span class="glyphicon glyphicon-plus"></span> Add admin
                                                </button>
                                                </div>
      
      <div class="col-md-8 col-md-offset-2">
      <%
         if(request.getAttribute("CannotDelete")!=null) {
         %>
      <div class="alert alert-danger">
         <%=request.getAttribute("CannotDelete")%>
      </div>
      <%
         }
         %>
      <div class="panel panel-primary">
         <div class="panel-heading">
            <h1>Administration</h1>
         </div>
         <div class="panel-body"></div>
         <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="assessments">
               <div class="row">
                  <form action="AssessmentServlet" method="get">
                     <div class="col-md-8 col-md-offset-1">
                        <input type="text" class="form-control"
                           placeholder="Search for an assessment name"
                           name="searchTextForAssessment" id="searchText"
                           autocomplete="off"  value='<%=request.getAttribute("SearchValue")%>'/>
                     </div>
                     <div class="col-md-3">
                        <button id="SearchBTN" class="btn btn-primary">Search</button>
                  </form>
                  <button type="button" class="btn btn-success" onclick="openPage('CreateAssessment.jsp')">
                  <span class="glyphicon glyphicon-plus"></span> Create
                  </button>
                  </div>
               </div>
               <br>
               <br>
               <div>
                  <table class="table table-hover" cellspacing="0" rules="all" border="1" style="border-collapse: collapse; table-layout: fixed; word-wrap: break-word;">
                     <tbody>
                        <tr>
                           <th class="bg-primary" scope="col">Assessment Name</th>
                           <th class="bg-primary" scope="col">Assessment Description</th>
                           <th class="bg-primary" scope="col">&nbsp;</th>
                        </tr>
                        <%
                           List<AssessmentBean> data = new ArrayList();
                           
                           													data = (List<AssessmentBean>)request.getAttribute("allAssessmentsList");
                           															for (AssessmentBean ab: data) {
                           																int assessmentID;
                           %>
                        <tr>
                           <td style="display: none;">
                              <%
                                 assessmentID=ab.getAssessmentID();
                                 %>
                           </td>
                           <td>
                              <%=ab.getAssessmentName()%>
                           </td>
                           <td>
                              <%=ab.getAssessmentDesc()%>
                           </td>
                           <td>
                              <div>
                                 <form action="SessionServlet" method="post">
                                    <button class="btn btn-primary glyphicon glyphicon-calendar pull-left" value="<%=assessmentID%>" name="sessionButton">Sessions
                                    </button>
                                 </form>
                                 <button onclick="callConfirmDelete(this.value)" class="btn btn-danger glyphicon glyphicon-trash pull-left" value="<%=assessmentID%>" name="archiveButton">Delete
                                 </button>
                              </div>
                           </td>
                        </tr>
                        <%
                           }
                           %>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Confirm Delete</h4>
               </div>
               <div class="modal-body">
                  <p>Do you want to delete this Assessment?</p>
               </div>
               <div class="modal-footer">
                  <button id="btnYes" class="btn danger">Yes</button>
                  <button data-dismiss="modal" aria-hidden="true" id="btnNo" class="btn secondary">No</button>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>