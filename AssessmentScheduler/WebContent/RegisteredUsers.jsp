<%@page trimDirectiveWhitespaces="true" %>
<%@page language="java" import="java.sql.Timestamp"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page language="java" import="com.mvc.bean.UserBean"%>
<%@page language="java" import="com.mvc.bean.SessionBean"%>
<%@page language="java" import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
      <script type="text/javascript">
         <%List<UserBean> list = new ArrayList();
            list = (List<UserBean>)request.getAttribute("registeredUsersList");
            List<SessionBean> sessionlist = new ArrayList();
            sessionlist = (List<SessionBean>)request.getAttribute("sessionDetails");
            String sessionstart="";
            String assessmentname="";
            for(UserBean ub: list){
            	sessionstart = ub.getSessionStart().toString().trim();
            	sessionstart = sessionstart.substring(0, sessionstart.length() -2);
            	assessmentname = ub.getAssessmentName()+"- "+sessionstart;
            	}
            %>
         	function download_csv() {
         		var rows = 0;
         		var usersTableData = document.getElementById('usersTable');
         		numofRows = usersTableData.rows.length -1 ;
         		numofColInRow = usersTableData.rows[0].cells.length - 1;
         		var rowcsv = [ numofRows ];
         		for (rows == 0; rows <= numofRows; rows++) {
         			var col = 0;
         			var data = "";
         			for (col == 0; col <= (numofColInRow-1); col++) {
         				if (col != (numofColInRow-1)) {
         					data += usersTableData.rows[rows].cells[col].innerText + ",";
         				} else {
         					data += usersTableData.rows[rows].cells[col].innerText+"\n";
         				}
         			}
         			rowcsv[rows] = data
         		}
         		var csvContent = "data:text/csv;charset=utf-8,\t";
         		var rowcnt = 0;
         		for (rowcnt == 0; rowcnt <= rowcsv.length - 1; rowcnt++) {
         			csvContent += rowcsv[rowcnt];
         		}
         		var aname = "<%=assessmentname%>";
         		var encodedUri = encodeURI(csvContent);
         		var link = document.createElement("a");
         		link.setAttribute("href", encodedUri);
         		link.setAttribute("download", aname+".csv");
         		document.body.appendChild(link); // Required for FF
         		link.click(); // This will download the data file named "my_data.csv".
         		
         	}
      </script>
      <script>
         history.forward();
      </script>
   </head>
   <body>
      <%@include file="Header.html"%>
      <br>
      <br>
      <br>
      <br>
      <div class="container" id="MainContainer">
         <div class="row">
            <div id="MainContentContainer" class="col-md-12">
               <div class="row">
                  <div class="col-xs-12">
                     <h2 class="panel-heading"></h2>
                  </div>
               </div>
               <div class="bg-main">
                  <div class="container col-md-8 col-md-offset-2">
                     <div class="panel panel-primary">
                        <div class="panel-heading">
                           <%
                              String assessName="";
                              											String sessionStartDate="";
                              											for(SessionBean sb: sessionlist){
                              												assessName=sb.getAssessmentName();
                              												sessionStartDate=sb.getSessionStart().toString().trim();
                              												sessionStartDate=sessionStartDate.substring(0, sessionStartDate.length() - 2);
                              											}
                              %>
                           <h1>Registration of Users</h1>
                           <h3>
                              <em> <span id="MainContent_MeetingName"><%=assessName%></span></em>
                           </h3>
                           <h3>
                              <span id="MainContent_DateOfMeeting"><%=sessionStartDate%></span>
                           </h3>
                        </div>
                        <div class="tab-content">
                           <div class="col-sm-12">
                              <div class="col-sm-1"></div>
                              <div class="col-sm-9"
                                 style="background-color: aliceblue; border-radius: 25px">
                                 <div></div>
                                 <div class="col-sm-12"></div>
                              </div>
                              <div class="col-sm-2"></div>
                           </div>
                           <div class="col-md-9"></div>
                           <div class="col-md-3">
                              <br>
                              <button class="btn btn-success pull-right" onclick="download_csv()">Generate csv report</button>
                              <!-- <form action="SessionServlet">
                                 <input type="submit"
                                 	class="btn btn-success pull-right" name="generateCSVReport" value="generateCSVReport"/>
                                 	<span style="font-size: 1.3em;"
                                 		class="glyphicon glyphicon-list-alt"></span> &nbsp;Generate
                                 	CSV Report
                                 </form> -->
                              <br>
                           </div>
                           <br> <br> <br> <br>
                           <div>
                              <div>
                                 <table id="usersTable" class="table table-hover"
                                    cellspacing="0" rules="all" border="1" id="RegistrationsGV"
                                    style="border-collapse: collapse;">
                                    <thead>
                                       <tr>
                                          <th scope="col" style="color: White; background-color: #337AB7;">UserID</th>
                                          <th scope="col" style="color: White; background-color: #337AB7;">First Name</th>
                                          <th scope="col" style="color: White; background-color: #337AB7;">Last Name</th>
                                          <th scope="col" style="color: White; background-color: #337AB7;">Registered Date</th>
                                          <th style="display: none;">Building Code</th>
                                          <th style="display: none;">Room Number</th>
                                          <th style="display: none;">Session Start</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr>
                                          <%
                                             for(UserBean ub: list){
                                             %>
                                          <%HttpSession userSession = request.getSession(false); %>
                                          <td><%=userSession.getAttribute("username")%></td>
                                          <td><%=ub.getFirstName()%></td>
                                          <td><%=ub.getLastName()%></td>
                                          <td><%=new SimpleDateFormat("MM-dd-yyyy HH:mm:ss").format(ub.getRegistrationDate())%></td>
                                          <td style="display: none;"><%=ub.getBuildingCode() %></td>
                                          <td style="display: none;"><%=ub.getRoomNumber() %></td>
                                          <td style="display: none;"><%=new SimpleDateFormat("MM-dd-yyyy HH:mm:ss").format(ub.getSessionStart()) %></td>
                                       </tr>
                                       <%
                                          }
                                          %>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                           <div>
                              <br>
                              <form action="SessionServlet" method="post">
                                 <%
                                    for (SessionBean sb: sessionlist){
                                    %>
                                 <button class="btn btn-defualt"
                                    value="<%=sb.getAssessmentID()%>" name="sessionButton"
                                    style="margin-left: 15px">Back</button>
                                 <%
                                    break;
                                    															}
                                    %>
                              </form>
                           </div>
                           <br>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>