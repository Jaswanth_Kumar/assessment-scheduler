package com.mvc.bean;

import java.sql.Timestamp;

public class UserBean extends SessionBean {
	private String userID;
	private String lastName;
	private String firstName;
	private Timestamp registrationDate;

	public UserBean() {
		super();
	}

	public UserBean(int sessionID, Timestamp sessionStart,
			Timestamp sessionEnd, Timestamp dateModified,
			Timestamp dateCreated, int numOfParticipants,
			int numOfParticipantsEnrolled, String buildingCode,
			String roomNumber, int assessmentID, String assessmentName,
			String assessmentDesc, Timestamp assessDateCreated,
			Timestamp assessDateModified, String userID, String lastName,
			String firstName, Timestamp registrationDate) {
		super(sessionID, sessionStart, sessionEnd, dateCreated, dateModified,
				numOfParticipants, numOfParticipantsEnrolled, buildingCode,
				roomNumber, assessmentID, assessmentName, assessmentDesc,
				assessDateCreated, assessDateModified);
		this.userID = userID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.registrationDate = registrationDate;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Timestamp getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Timestamp registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Override
	public String toString() {
		return super.toString() + "UserBean [userID=" + userID + ", lastName="
				+ lastName + ", firstName=" + firstName + ", registrationDate="
				+ registrationDate + "]";
	}

}
